//
//  Request.swift
//  CountryApp
//
//  Copyright © 2021 Enrique. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import ObjectMapperAdditions
import ObjectMapper_Realm
import RealmSwift

func getCountries(completion: @escaping([Countries]?, String?) -> Void) {
    Alamofire.request("\(Constants.URL_API)/all", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseArray { (response: DataResponse<[Countries]>) in
            let statusCode = response.response?.statusCode ?? 0
            switch (response.result) {
                case .success:
                     if statusCode < 300 {
                         if let response = response.result.value {
                            completion(response, nil)
                         } else {
                             completion(nil, "")
                         }
                     }else if statusCode == 404 {
                        completion(nil, "")
                     }
                     else{
                        completion(nil,"")
                     }
                     break
               case .failure(let error):
                    if error._code == NSURLErrorTimedOut {
                        print("Request timeout")
                    }else{
                        print("Error Desconocido")
                    }
                    break
            }
    }
}
