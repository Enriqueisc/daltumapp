//
//  TextDetailCell.swift
//  CountryApp
//
//  Created by Kapital on 03/03/21.
//  Copyright © 2021 Enrique. All rights reserved.
//

import Foundation
import UIKit
class TextDetailCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!

    func configure(title: String, text: String) {
        self.title.text = title
        self.desc.text = text
    }
}
