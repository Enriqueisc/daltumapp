//
//  CountryTableViewCell.swift
//  CountryApp
//
//  Created by Kapital on 03/03/21.
//  Copyright © 2021 Enrique. All rights reserved.
//

import UIKit
import Kingfisher
import SVGKit


class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var flag: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setupCell(country: Countries){
        self.name.text = "\(country.name)"
        self.flag.layer.cornerRadius = self.flag.frame.width / 2
        //let processor = SVGProcessor(size: CGSize(width: 45, height: 45))
        self.flag.kf.setImage(with: URL(string: country.getUrlImage()))
        
        
    
        
        /*guard let url = URL.init(string: country.flag) else {
                return
            }
            let resource = ImageResource(downloadURL: url)

            KingfisherManager.shared.retrieveImage(with: resource, options: [.processor(processor), .forceRefresh], progressBlock: nil) { result in
                switch result {
                case .success(let value):
                    print("Image: \(value.image). Got from: \(value.cacheType)")
                    self.flag.image = value.image
                case .failure(let error):
                    print("Error: \(error)")
                }
            }*/
    }
    
    
    
}
