import Foundation
import ObjectMapper
import RealmSwift
import ObjectMapperAdditions
import ObjectMapper_Realm
import MapKit


class Capital: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var info: String

    init(title: String, coordinate: CLLocationCoordinate2D, info: String) {
        self.title = title
        self.coordinate = coordinate
        self.info = info
    }
}


class Countries: Object, Mappable {
    @objc dynamic var name: String = ""
    var topLevelDomain =  List<String>()
    @objc dynamic var alpha2Code: String = ""
    @objc dynamic var alpha3Code: String = ""
    var callingCodes =  List<String>()
    @objc dynamic var capital: String = ""
    var languages = List<Language>()
    @objc dynamic var region: String = ""
    @objc dynamic var subregion: String = ""
    @objc dynamic var population: Int = 0
    var latlng = List<Double>()
    @objc dynamic var demonym: String = ""
    @objc dynamic var area: Int = 0
    var timezones = List<String>()
    var borders = List<String>()
    @objc dynamic var nativeName: String = ""
    @objc dynamic var numericCode: String = ""
    var currencies = List<Currency>()
    @objc dynamic var translations: Translation?
    @objc dynamic var flag: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.name <- map["name"]
        self.topLevelDomain <- (map["topLevelDomain"],RealmTransform())
        self.alpha2Code <- map["alpha2Code"]
        self.alpha3Code <- map["alpha3Code"]
        self.callingCodes <- (map["callingCodes"],RealmTransform())
        self.capital <- map["capital"]
        self.languages <- (map["languages"], ListTransform<Language>())
        self.region <- map["region"]
        self.subregion <- map["subregion"]
        self.population <- map["population"]
        self.latlng <- (map["latlng"],RealmTransform())
        self.demonym <- map["demonym"]
        self.area <- map["area"]
        self.timezones <- (map["timezones"],RealmTransform())
        self.borders <- (map["borders"],RealmTransform())
        self.nativeName <- map["nativeName"]
        self.numericCode <- map["numericCode"]
        self.currencies <- (map["currencies"], ListTransform<Currency>())
        self.translations <- map["translations"]
        self.flag <- map["flag"]
    }
    
    
    func getUrlImage()-> String {
        return "https://flagcdn.com/60x45/\(self.alpha2Code.lowercased()).png"
    }
    
    
    func getArrayString(key: String) -> String{
        if key == "topLevelDomain" {
            return topLevelDomain.joined(separator: ", ")
        }else if key == "callingCodes"{
            return callingCodes.joined(separator: ", ")
        }else if key == "timezones"{
            return timezones.joined(separator: ", ")
        }else if key == "borders"{
            return borders.joined(separator: ", ")
        }else{
            return ""
        }
    }
    
    func getArrayInt(key: String) -> String{
        if key == "latlng" {
            return self.latlng.reduce("") { $1 == nil ? $0 : $0.isEmpty ? "\($1)" : $0 + ",\($1)" }
        }else{
            return ""
        }
    }
    
    func getArrayObject(key: String) -> String{
        if key == "languages" {
            return languages.map({$0.name}).reduce("", {result, name in return result == "" ? "\(name)" : "\(result),\(name)"})
        }else if key == "currencies"{
            return currencies.map({$0.name}).reduce("", {result, name in return result == "" ? "\(name)" : "\(result),\(name)"})
        }else{
            return ""
        }
    }
}



var dataCountry: [KeysName] = [
    KeysName(name: "name", traslate: "Nombre", typeData: "String"),
    KeysName(name: "topLevelDomain", traslate: "Dominio", typeData: "ArrayString"),
    KeysName(name: "alpha3Code", traslate: "Alfa Code", typeData: "String"),
    KeysName(name: "callingCodes", traslate: "Código de marcación", typeData: "ArrayString"),
    KeysName(name: "capital", traslate: "Capital", typeData: "String"),
    KeysName(name: "languages", traslate: "Lenguajes", typeData: "ArrayObject"),
    KeysName(name: "region", traslate: "Region", typeData: "String"),
    KeysName(name: "subregion", traslate: "Subregión", typeData: "String"),
    KeysName(name: "population", traslate: "Población", typeData: "String"),
    KeysName(name: "latlng", traslate: "Ubicación", typeData: "ArrayInt"),
    KeysName(name: "demonym", traslate: "Nacionalidad", typeData: "String"),
    KeysName(name: "area", traslate: "Area", typeData: "String"),
    KeysName(name: "timezones", traslate: "Zonas Horarias", typeData: "ArrayString"),
    KeysName(name: "borders", traslate: "Frontera", typeData: "ArrayString"),
    KeysName(name: "nativeName", traslate: "Nombre nativo", typeData: "String"),
    KeysName(name: "numericCode", traslate: "Código Numérico", typeData: "String"),
    KeysName(name: "currencies", traslate: "Moneda", typeData: "ArrayObject"),
    KeysName(name: "translations", traslate: "Traducciones", typeData: "Object")
]



class Language: Object, Mappable {
    @objc dynamic var iso639_1: String = ""
    @objc dynamic var iso639_2: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var nativeName: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.iso639_1 <- map["iso639_1"]
        self.iso639_2 <- map["iso639_2"]
        self.name <- map["name"]
        self.nativeName <- map["nativeName"]
    }
}


class Currency: Object, Mappable {
    @objc dynamic var code: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var symbol: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.code <- map["code"]
        self.name <- map["name"]
        self.symbol <- map["symbol"]
    }
}


class Translation: Object, Mappable {
    @objc dynamic var de: String = ""
    @objc dynamic var es: String = ""
    @objc dynamic var fr: String = ""
    @objc dynamic var ja: String = ""
    @objc dynamic var it: String = ""
    @objc dynamic var br: String = ""
    @objc dynamic var pt: String = ""
    @objc dynamic var nl: String = ""
    @objc dynamic var hr: String = ""
    @objc dynamic var fa: String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        self.de <- map["de"]
        self.es <- map["es"]
        self.fr <- map["fr"]
        self.ja <- map["ja"]
        self.it <- map["it"]
        self.br <- map["br"]
        self.pt <- map["pt"]
        self.nl <- map["nl"]
        self.hr <- map["hr"]
        self.fa <- map["fa"]
    }
}

struct KeysName {
    var name: String
    var traslate: String
    var typeData: String
}

