//
//  DetailCountryViewController.swift
//  CountryApp
//
//  Created by Kapital on 03/03/21.
//  Copyright © 2021 Enrique. All rights reserved.
//

import UIKit
import MapKit
import Kingfisher

class DetailCountryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var country: Countries?
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var logoFlag: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.mapView.delegate = self
        let cty = Capital(title: "\(country?.name ?? "")", coordinate: CLLocationCoordinate2D(latitude: country?.latlng[0] ?? 51.507222, longitude: country?.latlng[0] ?? -0.1275), info: "\(country?.alpha3Code ?? "")")
        let initialLocation = CLLocation(latitude: country?.latlng[0] ?? 51.507222, longitude: country?.latlng[0] ?? -0.1275)
        self.centerMapOnLocation(location: initialLocation, cty: cty)
        self.logoFlag.layer.cornerRadius = self.logoFlag.frame.width / 2
        self.logoFlag.kf.setImage(with: URL(string: "\(country?.getUrlImage() ?? "")"))
        self.title = country?.name ?? ""
    }
    
    func centerMapOnLocation(location: CLLocation, cty:Capital) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.25, longitudeDelta: 0.25))
        DispatchQueue.main.async {
            self.mapView.setRegion(region, animated: true)
            self.mapView.addAnnotation(cty)
        }
    }
    
    func openMapButtonAction(latitude: Double,  longitude: Double) {
            let appleURL = "http://maps.apple.com/?daddr=\(latitude),\(longitude)"
            let googleURL = "comgooglemaps://?daddr=\(latitude),\(longitude)&directionsmode=driving"
            let wazeURL = "waze://?ll=\(latitude),\(longitude)&navigate=false"

            let googleItem = ("Google Map", URL(string:googleURL)!)
            let wazeItem = ("Waze", URL(string:wazeURL)!)
            var installedNavigationApps = [("Apple Maps", URL(string:appleURL)!)]

            if UIApplication.shared.canOpenURL(googleItem.1) {
                installedNavigationApps.append(googleItem)
            }

            if UIApplication.shared.canOpenURL(wazeItem.1) {
                installedNavigationApps.append(wazeItem)
            }

            let alert = UIAlertController(title: "Selecciona App", message: "¿Donde desea navegar?", preferredStyle: .actionSheet)
            for app in installedNavigationApps {
                let button = UIAlertAction(title: app.0, style: .default, handler: { _ in
                    UIApplication.shared.open(app.1, options: [:], completionHandler: nil)
                })
                alert.addAction(button)
            }
            let cancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
            alert.addAction(cancel)
            present(alert, animated: true)
    }
    
}


extension DetailCountryViewController: MKMapViewDelegate {

   func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
      print("calloutAccessoryControlTapped")
   }

   func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        openMapButtonAction(latitude: mapView.annotations.first?.coordinate.latitude ?? 0.0, longitude: mapView.annotations.first?.coordinate.latitude ?? 0.0)
   }
}

extension DetailCountryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextDetailCell", for: indexPath) as! TextDetailCell
        let countries = dataCountry[indexPath.row]
        let key = "\(countries.name)"
        if countries.typeData == "String"{
            cell.configure(title: "\(countries.traslate)", text: "\(country?[key] ?? "")")
        }else if countries.typeData == "ArrayString"{
            cell.configure(title: "\(countries.traslate)", text: "\(country?.getArrayString(key: key) ?? "")")
        }
        else if countries.typeData == "ArrayObject"{
            cell.configure(title: "\(countries.traslate)", text: "\(country?.getArrayObject(key: key) ?? "")")
        }
        else if countries.typeData == "ArrayInt"{
            cell.configure(title: "\(countries.traslate)", text: "\(country?.getArrayInt(key: key) ?? "")")
        }
        else{
            if let comp = country?[key] as? Translation {
                let list = "\(comp.de), \(comp.es), \(comp.fr), \(comp.ja), \(comp.it), \(comp.br), \(comp.pt), \(comp.nl), \(comp.nl), \(comp.hr), \(comp.fa)"
                cell.configure(title: "\(countries.traslate)", text: "\(list)")
            }else{
                cell.configure(title: "\(countries.traslate)", text: "\(country?[key] ?? "")")
            }
        }
        
        return cell
    }

}
