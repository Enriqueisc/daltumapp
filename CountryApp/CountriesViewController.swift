//
//  CountriesViewController.swift
//  CountryApp
//
//  Created by Kapital on 02/03/21.
//  Copyright © 2021 Enrique. All rights reserved.
//

import UIKit
import PKHUD



class CountriesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let searchController = UISearchController(searchResultsController: nil)
    var countries: [Countries] = []
    var countriesFiltered: [Countries] = []
    var filtersEnabled: Bool = false
    var textFilter: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        // Do any additional setup after loading the view.
        self.tableView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryTableViewCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        listCountries()
        setupSearchController()
    }
    
    
    func setupSearchController(){
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar..."
        self.searchController.searchBar.showsSearchResultsButton = false
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = self.searchController
        } else {
            // Fallback on earlier versions
        }
        definesPresentationContext = true
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "to_detail"{
            guard let destination = segue.destination as? DetailCountryViewController else {
                fatalError("No existe el controlador")
            }
            guard let country = sender as? Countries  else {
                fatalError("No existe el dato")
            }
            destination.country = country
            
        }else{
            print("No existe segue")
        }
    }
    
    func listCountries(){
        getCountries(){results, error in
            if error == nil {
                if let cts = results{
                    HUD.flash(.success, delay: 0.0)
                    self.countries = cts
                    self.tableView.reloadData()
                }else{
                    HUD.flash(.error, delay: 1.0)
                    self.countries = []
                    self.tableView.reloadData()
                }
            }else{
                print("Error: ", error)
                HUD.flash(.error, delay: 1.0)
                self.countries = []
                self.tableView.reloadData()
            }
        }
    }
    
    func searchBarIsEmpty() -> Bool {
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    
    
    func isFiltering() -> Bool {
        if self.searchController.isActive && !searchBarIsEmpty() {
            return true
        } else if self.filtersEnabled {
            return true
        } else {
            return false
        }
    }

}


extension CountriesViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let applyFilters = self.countriesFiltered
        return isFiltering() ? applyFilters.count : self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell", for: indexPath) as! CountryTableViewCell
        let applyFilters = self.countriesFiltered
        let country = isFiltering() ? applyFilters[indexPath.row] : self.countries[indexPath.row]
        cell.setupCell(country: country)
        cell.alpha = 0.25
        UIView.animate(withDuration: 0.5, animations: {
            cell.alpha = 1
        })
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let cellCount = tableView.visibleCells.count
        let alphaInterval:CGFloat = 1.0 / CGFloat(cellCount / 2)

        for (index,cell) in (tableView.visibleCells as [UITableViewCell]).enumerated() {
            if index < cellCount / 2 {
                cell.alpha = 1//CGFloat(index) * alphaInterval
            } else {
                cell.alpha = CGFloat(cellCount - index) * (alphaInterval)
            }
        }
    }
    
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        //Scroll up velocity
        if(velocity.y < 0){
            if self.textFilter.isEmpty {
                //Ocultar el teclado para visualizar los paises en la lista
                self.searchController.searchBar.endEditing(true)
            }else{
                print("Tiene filtro de busqueda y hace scroll")
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let applyFilters = self.countriesFiltered
        let country = isFiltering() ? applyFilters[indexPath.row] : self.countries[indexPath.row]
        navigateDetail(country: country)
    }
    
    
    func navigateDetail(country: Countries){
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "to_detail", sender: country)
        }
    }
    
}


extension CountriesViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            if isFiltering(){
                if text.isEmpty{
                    self.textFilter = ""
                    print("Consultar los paises: ", text)
                    self.tableView.reloadData()
                }else{
                    print("Consultar los paises filtrados: ", text)
                    self.textFilter = text
                    self.countriesFiltered = self.countries.filter({$0.name.lowercased().contains(text.lowercased())})
                    self.tableView.reloadData()
                }
            }else{
                self.textFilter = ""
                print("Consultar los paises")
                self.tableView.reloadData()
            }
        }
    }
}
